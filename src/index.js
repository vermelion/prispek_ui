import React from 'react';
import ReactDOM from 'react-dom';
import { ConfigContextProvider } from './Config/ConfigContex';
import { EventsContextProvider } from './Config/Events/EventsContext';
import reducer, { initialState } from './Config/ConfigReducer';
import ThemeContextProvider from './Config/Theme/Theme';
import App from './App';

ReactDOM.render(
  <ConfigContextProvider initialState={initialState} reducer={reducer}>
    <EventsContextProvider>
      <ThemeContextProvider>
        <App />
      </ThemeContextProvider>
    </EventsContextProvider>
  </ConfigContextProvider>,
  document.getElementById('root')
);
