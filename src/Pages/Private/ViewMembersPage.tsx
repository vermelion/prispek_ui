import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useAlert } from 'react-alert';
import AdminNavbar from '../../Components/Private/AdminNavbar';
import { useConfig } from '../../Config/ConfigContex';
import { useTheme } from '../../Config/Theme/Theme';
import ViewMembersTable from '../../Components/Private/ViewMembersTable';
import { FormControlLabel, IconButton } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import AddMembers from '../../Components/Private/AddMembers';

const ViewEmployeesPage = () => {
  const alert = useAlert();
  const { primary } = useTheme();
  const [{ route, user, viewMember }, dispatch] = useConfig();
  const [employees, setEmployees] = useState([]);
  const [admin, setAdmin] = useState([]);
  const [response, setResponse] = useState(false);
  const [addMember, setAddMember] = useState(false);
  const [getMembers, setGetMembers] = useState(false);
  useEffect(() => {
    async function getMembers() {
      await axios
        .get(
          `${route}/dbViewMembers?member_guid=${user.MemberGuid}&session_guid=${user.SessionGuid}`
        )
        .then(res => {
          if (res.data.result === 'error') {
            alert.error(res.data.frontEndMessage);
          } else {
            setAdmin(res.data.data[0]);
            setEmployees(res.data.data[1]);
            setResponse(true);
          }
        })
        .catch(error => console.log(error));
    }
    getMembers();
  }, [getMembers, dispatch, route, user, alert]);
  return (
    <div className='section'>
      <AdminNavbar view />
      <ViewMembersTable
        getMembers={getMembers}
        setGetMembers={setGetMembers}
        response={response}
        dispatch={dispatch}
        viewMember={viewMember}
        primary={primary}
        employees={employees}
        admin={admin}
      />
      {addMember ? (
        viewMember === 0 ? (
          <AddMembers
            employee
            getMembers={getMembers}
            setGetMembers={setGetMembers}
            setAddMember={setAddMember}
          />
        ) : (
          <AddMembers
            getMembers={getMembers}
            setGetMembers={setGetMembers}
            setAddMember={setAddMember}
          />
        )
      ) : (
        <FormControlLabel
          style={{ marginTop: '1.5rem' }}
          control={
            <IconButton
              style={{ marginRight: '0.5rem' }}
              onClick={() => setAddMember(true)}
            >
              <Add />
            </IconButton>
          }
          label={viewMember === 0 ? 'Add Employee' : 'Add Admin'}
        />
      )}
    </div>
  );
};

export default ViewEmployeesPage;
