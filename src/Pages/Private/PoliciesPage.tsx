import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AdminNavbar from '../../Components/Private/AdminNavbar';
import PolicyPDF from '../../Components/Private/PolicyPDF';
import pdf from '../../Assets/pdfs/policies/THE-GUIDE.pdf'

const PoliciesPage = () =>  (
    <div className='section'>
      <h1 style={{ marginBottom: '1rem' }}>Workplace Policies</h1>
      <AdminNavbar policies />
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <Typography>THE-GUIDE-FOR-THE-BEREAVED</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <PolicyPDF pdf={pdf} title='The Guide' />
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <Typography>THE-GUIDE-FOR-THE-BEREAVED 02</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <PolicyPDF pdf={pdf} title='The Guide' />
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <Typography>THE-GUIDE-FOR-THE-BEREAVED 03</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <PolicyPDF pdf={pdf} title='The Guide' />
        </AccordionDetails>
      </Accordion>
    </div>
  );

export default PoliciesPage;
