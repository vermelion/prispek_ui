import React from 'react';

import Navbar from '../../Components/Public/Navbar';
import LoginForm from '../../Components/Private/LoginForm';

const LoginPage = () => (
  <div className='admin-section'>
    <Navbar login />
    <LoginForm />
  </div>
);

export default LoginPage;
