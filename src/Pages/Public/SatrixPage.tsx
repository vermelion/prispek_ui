import React, { useEffect } from 'react';
import '../../Assets/sass/satrix.scss';
import Footer from '../../Components/Footer/Footer';
import Navbar from '../../Components/Public/Navbar';
import { useTheme } from '../../Config/Theme/Theme';

const SatrixPage = () => {
  const { primary } = useTheme();
  useEffect(() => {
    window.onload = () => {
      window.scrollTo(0, 0);
    };
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <div className='section'>
        <Navbar />
        <div className='satrix'>
          <h2 className='satrix__title'>Satrix</h2>
          <section className='satrix__section'>
            <h4 className='satrix__section__title'>Satrix Investing</h4>
            <p className='satrix__section__paragraph'>
              You can trade SATRIX investments, hassle free on our website if
              you prefer to do it directly. Anytime, any place!
            </p>
            <p className='satrix__section__paragraph'>
              Complete the following documents at your leasure, sign and return
              it to{' '}
              <a
                href='/'
                target='_blank'
                rel='noopener noreferrer'
                className='satrix__section__paragraph__link'
                style={{ color: primary }}
              >
                service@prispek.co.za
              </a>{' '}
              for us to provide our famous intermediary service to impliment
              your wishes!
            </p>
          </section>
          <section
            className='satrix__section'
            style={{ background: '#fdeec3' }}
          >
            <span className='b left--top' />
            <span className='satrix__section__line top--left' />
            <span className='satrix__section__line top--right' />
            <span className='satrix__section__line right--top' />
            <span className='satrix__section__line right--bottom' />
            <span className='satrix__section__line bottom--right' />
            <span className='satrix__section__line bottom--left' />
            <span className='satrix__section__line left--bottom' />
            <h4 className='satrix__section__title'>Forms:</h4>
            <ul className='satrix__section__list'>
              <h5 className='satrix__section__list__title'>Purchases</h5>
              <li className='satrix__section__list__item'>
                <a
                  href='http://prispek.co.za/wp-content/uploads/2018/08/1.-Unit-Trust-Application-Form-Individual-Investors.pdf'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  Unit Trust Application Form – Individual Investors (NEW
                  INVESTOR)
                </a>
              </li>
              <li className='satrix__section__list__item'>
                <a
                  href='http://prispek.co.za/wp-content/uploads/2018/08/2.-Unit-Trust-Additional-Investment-Form.pdf'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  Unit Trust Additional Investment Form (EXISTING INVESTOR)
                </a>
              </li>
            </ul>
            <ul className='satrix__section__list'>
              <h5 className='satrix__section__list__title'>Repurchases</h5>
              <li className='satrix__section__list__item'>
                <a
                  href='http://prispek.co.za/wp-content/uploads/2018/08/3.-Unit-Trust-redemption-form-Satrix.pdf'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  Unit Trust redemption form – Satrix
                </a>
              </li>
            </ul>
            <h4
              className='satrix__section__title'
              style={{ marginTop: '4rem' }}
            >
              Fund Selection:
            </h4>
            <p className='satrix__section__paragraph'>
              Choose from the{' '}
              <a
                href='https://satrix.co.za/products'
                className='satrix__section__paragraph__link'
                style={{ color: primary }}
              >
                following funds
              </a>
            </p>
          </section>
          {/* <section
            className='satrix__section background'
            style={{ background: '#fdeec3' }}
          >
            <span className='satrix__section__line left small' />
            <span className='satrix__section__line top small' />
            <span className='satrix__section__line right small' />
            <span className='satrix__section__line bottom small' />
          </section> */}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default SatrixPage;
