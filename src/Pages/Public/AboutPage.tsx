import React, { useEffect, useRef } from 'react';
import '../../Assets/sass/about.scss';
import Navbar from '../../Components/Public/Navbar';
import Footer from '../../Components/Footer/Footer';
import AboutCard from '../../Components/Public/About/AboutCard';
import PRISPEK from '../../Assets/images/PRISPEK.png';
import SPEKTRA from '../../Assets/images/SPEKTRA.png';
import PRISMA from '../../Assets/images/PRISMA.jpg';
import Team from '../../Components/Public/About/Team';
import { useEvents } from '../../Config/Events/EventsContext';
// import { useConfig } from '../../Config/ConfigContex';

const AboutPage = () => {
  // const [{ readMore }] = useConfig();
  const { aboutLoad } = useEvents();
  const prispekRef = useRef<HTMLElement>(null);
  const spektraRef = useRef<HTMLElement>(null);
  const prismaRef = useRef<HTMLElement>(null);
  useEffect(() => {
    switch (aboutLoad) {
      case 'Prispek':
        window.scrollTo(
          0,
          prispekRef.current?.offsetTop ? prispekRef.current?.offsetTop - 25 : 0
        );
        break;
      case 'Spektra':
        window.scrollTo(
          0,
          spektraRef.current?.offsetTop ? spektraRef.current?.offsetTop - 25 : 0
        );
        break;
      case 'Prisma':
        window.scrollTo(
          0,
          prismaRef.current?.offsetTop ? prismaRef.current?.offsetTop - 25 : 0
        );
        break;
      default:
        window.scrollTo(0, 0);
        break;
    }
    window.onload = () => {
      window.scrollTo(0, 0);
    };
  }, [aboutLoad]);
  return (
    <>
      <Navbar about={true} />
      <div className='section'>
        <Team />
        <div className='about'>
          <section ref={prispekRef}>
            <AboutCard
              logo={PRISPEK}
              title='Prispek'
              about={[
                'Christo Viljoen &amp; Gerhard Labuschagne started a partnership in 1996 to address the needs of small to medium size businesses regarding sound financial advice and accounting services. Providing a one-stop point of professional advice and giving valuable back office accounting support offered clients the unique opportunity to focus on their primary areas of expertise whilst knowing that the financial planning and implementation ran smoothly behind the scenes. This advice and service was to be given with INTEGRITY at all times.',
                'Following the early successes achieved by Spektra Makelaars and Prisma Rekenmeesters, Prispek Consulting was formed in 1999 to act as a single point of contact for clients wishing to be part of this solution and to provide proper succession planning for the members.  Clients make initial contact with Prispek Consulting, from where they are referred to either Spektra Makelaars or Prisma Rekenmeesters for the specialised services they need. Close relations created with other companies with other specialised services has led to unique, unbiased advice and services delivered to all clients, strengthening the desire to create wealth with integrity for their clients.',
                'Prispek Consulting has grown to a thriving business assisting clients with their wills and executorship with deceased estates, providing clients the peace of mind that their finances and businesses are taken care of right from the start until even after their deaths and proper distribution of their estates.'
              ]}
            />
          </section>
          <section ref={spektraRef}>
            <AboutCard
              logo={SPEKTRA}
              title='Spektra'
              about={[
                `Christo Viljoen started giving financial advice as an agent with Fedsure Life in 1996.  He realised however that in order to give the best advice for the benefit of every client, he has to become an independant advisor with no restrictions placed on his advice by insurance companies or banks.  Spektra brokers was registered in 1998 and together with Prisma accountants started giving unbiased advice to their clients.`,
                'Registered as an Authorised Financial Services Provider (FSP NO. 10679) with the Financial Services Board, with the Council for Medica Schemes (ORG 240) and is covered by professional indemnity insurance to protect clients.',
                'Christo Viljoen is registered with and subject to the professional code of conduct of The SAIFAA (South African Independent Financial Advisors Association), is licensed and has passes all written examinations as prescribed by the Financial Services Board enabling him to provide financial advice. he is also registered with the Council for Medical Schemes (BR295) and has contracts with various large and Financial Services Board approved Insurance and Asset Management Companies and has been offering financial advice and intermediary services since 1996.'
              ]}
              list={{
                title: 'Advice and intermediary services include:',
                items: [
                  { key: 0, name: 'Investment advice and services' },
                  { key: 1, name: 'Long term insurance and services' },
                  { key: 2, name: 'Medical Fund advice and services' }
                ]
              }}
              footer='Spektra Makelaars BK, 1998/067770/23, FSP 10679: An Authorised Financial Services Provider.'
            />
          </section>
          <section ref={prismaRef}>
            <AboutCard
              logo={PRISMA}
              title='Prisma'
              about={[
                'Gerhard Labuschagne registered in 1998 as the owner of Prisma Accountants with SAIPA (The South African Institute of Professional Accountants – NO. 7830); with SARS ( South African Revenue Services – NO. 0020236); as a tax practitioner.'
              ]}
              list={{
                title: 'Advice and services include:',
                items: [
                  { key: 3, name: 'Business Consultation' },
                  { key: 4, name: 'Establishment of new businesses' },
                  { key: 5, name: 'Accountancy services' },
                  { key: 6, name: 'Income Tax advice' },
                  { key: 7, name: 'Payroll administration and advice' },
                  { key: 8, name: 'Establishment and administration of Trusts' }
                ]
              }}
              footer='Prisma Accountants is also registered as an Approved Training Centre for trainee accountants (SAIPA no.48) '
            />
          </section>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default AboutPage;
