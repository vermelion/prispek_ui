import React, { useEffect, useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { useTheme } from '../../Config/Theme/Theme';
import '../../Assets/sass/contact.scss';
import Navbar from '../../Components/Public/Navbar';
import Footer from '../../Components/Footer/Footer';
import MapPDF from '../../Components/Public/Contact/MapPDF';
import BookForm from '../../Components/Public/Contact/BookForm';
import QueryForm from '../../Components/Public/Contact/QueryForm';
import FormCalendar from '../../Components/Public/Contact/FormCalendar';
import Contacts from '../../Components/Footer/Contacts';
import MAP from '../../Assets/images/Map.png';

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

const ContactPage = () => {
  const { background } = useTheme();
  const [value, setValue] = useState(0);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };
  return (
    <div>
      <Navbar contact={true} />
      <div className='section' style={{ background: background }}>
        <div className='contact--description'>
          <h1>Booking an Appointment</h1>
          <p>
            Our clients are always welcome to make an appointment to either
            visit us or make use of our boardroom or training facility.
          </p>
          <p>
            The boardroom has seating for 6 whilst the training room can easily
            accommodate up to 15 people for small seminars or group meetings
          </p>
        </div>
        <div className='contact--book'>
          <div className='contact--book__tabs'>
            <AppBar position='relative'>
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label='simple tabs example'
              >
                <Tab label='Book Appointment' {...a11yProps(0)} />
                <Tab label='Queries' {...a11yProps(1)} />
              </Tabs>
            </AppBar>
            {value === 0 && <BookForm />}
            {value === 1 && <QueryForm />}
          </div>
          <FormCalendar />
        </div>
        <div className='contact--details'>
          <Contacts page />
          <a
            href='https://www.google.com/maps/place/Prispek+Consulting/@-25.6873846,28.231015,15z/data=!4m5!3m4!1s0x0:0x6d737c2de80ddf05!8m2!3d-25.6868226!4d28.2322884'
            className='contact--details__map'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img src={MAP} alt='Prispek Maps' />
          </a>
        </div>
        <MapPDF />
      </div>
      <Footer />
    </div>
  );
};

export default ContactPage;
