import React, { useEffect } from 'react';

import '../../Assets/sass/landing/landing.scss';
import { useTheme } from '../../Config/Theme/Theme';
import Navbar from '../../Components/Public/Navbar';
import Footer from '../../Components/Footer/Footer';
import Header from '../../Components/Public/Landing/Header';
import Testimonials from '../../Components/Public/Landing/Testimonials';
import { useConfig } from '../../Config/ConfigContex';
import PRISPEK from '../../Assets/images/PRISPEK.png';
import SPEKTRA from '../../Assets/images/SPEKTRA.png';
import PRISMA from '../../Assets/images/PRISMA.jpg';
import AdminNavbar from '../../Components/Private/AdminNavbar';
import AboutCard from '../../Components/Public/About/AboutCard';

const LandingPage = () => {
  const { background } = useTheme();
  const [{ user, readMore }] = useConfig();
  useEffect(() => {
    !readMore && window.scrollTo(0, 0);
  }, [readMore]);
  return (
    <div>
      {user.MemberGuid ? <AdminNavbar landing /> : <Navbar landing />}
      <Header />
      <div className='section' style={{ background: background }}>
        <div className='landing__about'>
          <AboutCard
            readMore
            logo={PRISPEK}
            title='Prispek'
            about={[
              `Prispek Consulting comprises of three entities functioning strongly, both collectively and independently, to benefit you. Prisma Accountants &amp; Spektra Brokers form a strong bond with Prispek Consulting making it possible for clients to receive professional one-stop financial advice and service. Prispek Consulting conducts seminars and briefings to keep you informed about economic updates and relevant changes in tax legislation. One appointment is all it takes to discuss your tax, insurance and investment portfolio and any effects changes might have on your portfolio.`
            ]}
          />
          <AboutCard
            readMore
            logo={SPEKTRA}
            title='Spektra'
            about={[
              `Christo Viljoen started giving financial advice as an agent with Fedsure Life in 1996.  He realised however that in order to give the best advice for the benefit of every client, he has to become an independant advisor with no restrictions placed on his advice by insurance companies or banks.  Spektra brokers was registered in 1998 and together with Prisma accountants started giving unbiased advice to their clients.`
            ]}
          />
          <AboutCard
            readMore
            logo={PRISMA}
            title='Prisma'
            about={[
              'Gerhard Labuschagne registered in 1998 as the owner of Prisma Accountants with SAIPA (The South African Institute of Professional Accountants – NO. 7830); with SARS ( South African Revenue Services – NO. 0020236); as a tax practitioner.'
            ]}
          />
        </div>
        <Testimonials />
      </div>
      <Footer />
    </div>
  );
};

export default LandingPage;
