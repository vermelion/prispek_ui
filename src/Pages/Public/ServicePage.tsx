import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

import '../../Assets/sass/services.scss';
import Navbar from '../../Components/Public/Navbar';
import Footer from '../../Components/Footer/Footer';
import USER from '../../Assets/svgs/user.svg';
import LOGIN from '../../Assets/svgs/login.svg';
import CURRENCY from '../../Assets/svgs/currency.svg';
import EXCHANGE from '../../Assets/svgs/exchange.svg';
import ServicesCard from '../../Components/Public/ServicesCard';
import ServicesGoals from '../../Components/Public/ServicesGoals';

const ServicePage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
    window.onload = () => {
      window.scrollTo(0, 0);
    };
  }, []);
  return (
    <div>
      <Navbar services />
      <div className='section'>
        <div className='services--about'>
          <p className='services--about__offer'>
            <strong>Prispek Consulting</strong>, working closely with{' '}
            <strong> Prisma Accountants &amp; Spektra Brokers</strong>,  offers
            the following value proposition to our valued clients:
          </p>
          <ServicesGoals />
          <p className='services--about__how'>
            YOU CAN (A) BOOK AN APPOINTMENT OR (B) REQUEST A SERVICE ONLINE! OR
            YOU CAN FOLLOW YOUR INVESTMENT AND RISK PORTFOLIO ON THE CLIENT
            LOGIN BUTTON. Click on the tab or button below to initiate… BOOK AN
            APPOINTMENT ONLINE WITH:
          </p>
          <Button
            variant='contained'
            color='primary'
            style={{ marginRight: '1rem' }}
          >
            Prispek Consulting
          </Button>
          <Button variant='contained' color='secondary'>
            Spektra Makelaars
          </Button>
          <p className='services--about__how'>REQUEST ASSISTANCE FROM:</p>
          <a href='https://forms.office.com/Pages/ResponsePage.aspx?id=5IjpUQXKtUqHwZNdBKRHnhPuqyYmUKNMq0mUl44Yr4BUMEFCUVNJQjlHSlM5WEgzWVMyWU8yN0taSy4u'>
            <Button variant='contained' color='secondary'>
              Spektra Makelaars
            </Button>
          </a>
          <p className='services--about__how'>SATRIX ONLINE APPLICATIONS:</p>
          <Link to='/satrix'>
            <Button variant='contained' color='inherit'>
              SATRIX PAGE
            </Button>
          </Link>
          <p className='services--about__how'>
            Our specialised services include:
          </p>
        </div>
        <div className='services--cards'>
          <ServicesCard
            invert
            alt='Client Portfolio Icon'
            title='Client Portfolio'
            icon={USER}
            area='one'
          />
          <ServicesCard
            cta='Login'
            href='https://spektra.wealthportal.co.za'
            alt='Client Portfolio Second Icon'
            icon={LOGIN}
            area='two'
          />
          <ServicesCard
            invert
            alt='Foreign Currency Transaction Icon'
            title='Foreign Currency Transaction'
            icon={EXCHANGE}
            area='three'
          />
          <ServicesCard
            cta='Transact'
            alt='Foreign Currency Transaction Second Icon'
            href='https://spektra.wealthportal.co.za'
            icon={CURRENCY}
            area='four'
          />
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default ServicePage;
