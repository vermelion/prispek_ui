import React from 'react';
import '../../Assets/sass/blog.scss';
import Navbar from '../../Components/Public/Navbar';
import Footer from '../../Components/Footer/Footer';
import BlogPost from '../../Components/Public/BlogPost';
// import IMAGE from '../../../Assets/components/image.jpeg';

const BlogPage = () => (
  <>
    <Navbar blog={true} />
    <div className='section'>
      <BlogPost
        direction='column'
        title='Prispek Consulting'
        date='7 December 2020'
        media='https://external.fjnb7-1.fna.fbcdn.net/safe_image.php?d=AQHdTRQZl_vinwX7&w=500&h=261&url=https%3A%2F%2Fgallery.mailchimp.com%2Ff3054cb4c9a03dd8b9b8f5349%2Fimages%2F594e80ce-bf5c-4074-8369-4ff9f809c922.png&cfs=1&ext=jpg&_nc_cb=1&_nc_hash=AQHlpg2jLaCf601d'
        alt='Prispek Image'
        description='We wish all our clients a very blessed festive season with their families and loved ones.'
        link='This is the Link'
        href='https://www.facebook.com/prispek/posts/2721499621494926'
      />
    </div>
    <Footer />
  </>
);

export default BlogPage;
