import React from 'react';
import { Button } from '@material-ui/core';
import { Home } from '@material-ui/icons';
import BACKGROUND from '../Assets/images/not-found.jpg';

const NotFoundPage = () => (
  <div
    style={{
      height: '100vh',
      width: '100%',
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
      placeContent: 'center',
      placeItems: 'center'
    }}
  >
    <img
      style={{
        width: '100%',
        height: '100vh',
        position: 'fixed',
        zIndex: 0,
        objectFit: 'cover',
        objectPosition: 'center'
      }}
      src={BACKGROUND}
      alt='background'
    />
    <h1 style={{ zIndex: 1, marginBottom: 5 }}>404 Not Found</h1>
    <h2 style={{ zIndex: 1, marginBottom: 10 }}>ERROR</h2>
    <a href='https://www.nametours.co.za'>
      <Button
        variant='contained'
        color='primary'
        size='large'
        style={{ fontSize: '1.3rem', fontWeight: 800 }}
        startIcon={<Home />}
      >
        Home
      </Button>
    </a>
  </div>
);

export default NotFoundPage;
