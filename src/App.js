import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import AlertTemplate from 'react-alert-template-basic';
import { transitions, positions, Provider as AlertProvider } from 'react-alert';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { useTheme } from './Config/Theme/Theme';
import './main.scss';
import Routes from './Routes';
import { useConfig } from './Config/ConfigContex';

const App = () => {
  const { theme } = useTheme();
  const [{ user }] = useConfig();
  const muiTheme = createMuiTheme({
    palette: {
      primary: {
        main: theme.primary
      },
      secondary: {
        main: theme.secondary
      },
      default: {
        main: '#DFA558'
      }
    },
    typography: {
      h1: { fontSize: '3rem', fontWeight: 600 },
      h2: { fontSize: '2rem', fontWeight: 600 },
      h3: { fontSize: '1.8rem', fontWeight: 500 },
      h4: { fontSize: '1.5rem', fontWeight: 500 },
      caption: {
        cursor: 'context-menu',
        fontWeight: 700,
        fontSize: 'medium'
      }
    }
  });
  const alertOptions = {
    position: positions.BOTTOM_CENTER,
    timeout: 3000,
    offset: '30px',
    transition: transitions.SCALE
  };
  return (
    <ThemeProvider theme={muiTheme}>
      <AlertProvider template={AlertTemplate} {...alertOptions}>
        <BrowserRouter>
          <Routes user={user} />
        </BrowserRouter>
      </AlertProvider>
    </ThemeProvider>
  );
};

export default App;
