import React, { createContext, useContext, useState } from 'react';

export const EventsContext = createContext();

export const EventsContextProvider = ({ children }) => {
  const [aboutLoad, setAboutLoad] = useState('default');
  const [serviceLoad, setServiceLoad] = useState('default');
  const changeAboutLoad = title => {
    setAboutLoad(title);
  };
  const changeServiceLoad = title => {
    setServiceLoad(title);
  };
  const reset = () => {
    setAboutLoad('default');
  };
  return (
    <EventsContext.Provider
      value={{
        aboutLoad,
        changeAboutLoad: changeAboutLoad,
        serviceLoad,
        changeServiceLoad: changeServiceLoad,
        reset: reset
      }}
    >
      {children}
    </EventsContext.Provider>
  );
};

export const useEvents = () => useContext(EventsContext);
