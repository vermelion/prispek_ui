import axios from 'axios';

export default function getMembers(
  route: string,
  user: any,
  dispatch: any,
  alert: any,
  setMembers: any
) {
  axios
    .get(
      `${route}/dbViewMembers?member_guid=${user.MemberGuid}&session_guid=${user.SessionGuid}`
    )
    .then(res => {
      console.log('View Members: ', res);
      if (res.data.result === 'error') {
        alert.error(res.data.frontEndMessage);
      } else {
        setMembers(res.data.data);
        // dispatch({ type: 'SET_MEMBERS', members: res.data.data });
      }
    })
    .catch(error => console.log(error));
}
