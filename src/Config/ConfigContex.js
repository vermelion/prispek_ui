import React, { createContext, useContext, useReducer } from 'react';

export const ConfigContext = createContext();

export const ConfigContextProvider = ({ initialState, reducer, children }) => (
  <ConfigContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </ConfigContext.Provider>
);

export const useConfig = () => useContext(ConfigContext)