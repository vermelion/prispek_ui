export type DownloadType = {
  title: string;
  links: Array<{ href: string; name: string }>;
};
