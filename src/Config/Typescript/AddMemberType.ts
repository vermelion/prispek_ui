import { Dispatch, SetStateAction } from 'react';

export type AddMemberType = {
  employee?: boolean;
  getMembers: boolean;
  setGetMembers: Dispatch<SetStateAction<boolean>>;
  setAddMember: Dispatch<SetStateAction<boolean>>;
};
