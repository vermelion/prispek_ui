export type ThemeType = {
  primary: string;
  secondary: string;
  lightYellow: string;
  background: string;
  text: string;
  grey: string;
  cardBack: string;
  theme: object;
};
