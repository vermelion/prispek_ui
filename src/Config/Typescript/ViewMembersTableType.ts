import { Dispatch, SetStateAction } from 'react';

export type ViewMembersTableType = {
  getMembers: boolean;
  setGetMembers: Dispatch<SetStateAction<boolean>>;
  response: boolean;
  primary: any;
  employees: any;
  admin: any;
  viewMember: number;
  dispatch: any;
};
export type ViewMembersTableDataType = {
  getMembers: boolean;
  setGetMembers: Dispatch<SetStateAction<boolean>>;
  selectedMember: string | null;
  setSelectedMember: Dispatch<SetStateAction<string | null>>;
  memberRole: number;
  member: any;
};
export type ViewMembersTableHeadType = {
  sortBy: string;
  setSortBy: Dispatch<SetStateAction<string>>;
  sortState: boolean;
  setSortState: Dispatch<SetStateAction<boolean>>;
  primary: any;
};
