export type NavbarType = {
  landing?: boolean;
  about?: boolean;
  services?: boolean;
  blog?: boolean;
  contact?: boolean;
  login?: boolean;
  satrix?: boolean;
  view?: boolean;
  policies?: boolean;
};
