export type AboutCardType = {
  title: string;
  logo: string;
  about: Array<string>;
  read?: string;
  readMore?: boolean;
  list?: { title: string; items: Array<{ key: number; name: string }> };
  footer?: string;
};
