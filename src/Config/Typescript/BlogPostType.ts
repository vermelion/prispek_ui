export type BlogPostType = {
  direction: string;
  title: string;
  date: string;
  media: string;
  alt: string;
  description: string;
  link: string;
  href: string;
};
