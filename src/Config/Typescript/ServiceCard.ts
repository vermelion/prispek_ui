export type ServiceCardType = {
  invert?: boolean;
  icon: string;
  alt: string;
  title?: string;
  cta?: string;
  href?: string;
  area: string;
};
