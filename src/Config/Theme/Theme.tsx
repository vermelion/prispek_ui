import React, { createContext, useContext } from 'react';
import { ThemeType } from '../Typescript/ThemeType';

const ThemeContextProvider = ({ children }: any) => {
  const theme: ThemeType = {
    primary: '#731116',
    secondary: '#DFA558',
    lightYellow: '#FEE7A6',
    background: '#EEE',
    text: '#BBB',
    grey: '#ADAC9D',
    cardBack: '#334A6F',
    theme: {
      primary: '#731116',
      secondary: '#DFA558',
      lightYellow: '#FEE7A6',
      background: '#EEE',
      text: '#BBB',
      grey: '#ADAC9D',
      cardBack: '#334A6F'
    }
  };

  return (
    <ThemeContext.Provider value={{ ...theme }}>
      {children}
    </ThemeContext.Provider>
  );
};

export const ThemeContext = createContext<ThemeType>({} as ThemeType);

export const useTheme = () => useContext(ThemeContext);

export default ThemeContextProvider;
