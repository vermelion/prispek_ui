//TODO: The Initial State
export const initialState = {
  route: 'http://localhost:5000',
  user: { RoleType: 3 },
  viewMember: 0,
  members: [],
  readMore: false
};
//TODO: Reducer
const reducer = (state, action) => {
  console.log('ACTION: | ', action);
  switch (action.type) {
    case 'RESET':
      return initialState;
    case 'SET_USER':
      return {
        ...state,
        user: action.user
      };
    case 'SET_VIEW-MEMBER':
      return {
        ...state,
        viewMember: action.viewMember
      };
    case 'SET_MEMBERS':
      return {
        ...state,
        members: action.members
      };
    case 'SET_READ-MORE':
      return {
        ...state,
        readMore: action.readMore
      };
    default:
      return state;
  }
};
export default reducer;
