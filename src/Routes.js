import React from 'react';
import { Switch, Route } from 'react-router-dom';
import LandingPage from './Pages/Public/LandingPage';
import AboutPage from './Pages/Public/AboutPage';
import BlogPage from './Pages/Public/BlogPage';
import ContactPage from './Pages/Public/ContactPage';
import ServicePage from './Pages/Public/ServicePage';
import LoginPage from './Pages/Private/LoginPage';
import ViewEmployeesPage from './Pages/Private/ViewMembersPage';
import PoliciesPage from './Pages/Private/PoliciesPage';
import NotFoundPage from './Pages/NotFoundPage';
import SatrixPage from './Pages/Public/SatrixPage';

const Routes = ({ user }) => (
  <Switch>
    <Route path='/' component={LandingPage} exact />
    <Route path='/about' component={AboutPage} exact />
    <Route path='/blog' component={BlogPage} exact />
    <Route path='/contact' component={ContactPage} exact />
    <Route path='/services' component={ServicePage} exact />
    <Route path='/satrix' component={SatrixPage} exact />
    <Route path='/login' component={LoginPage} exact />
    {/* Private */}
    <Route
      path='/view-members'
      component={
        user.Role === 0 || user.Role === 1 ? ViewEmployeesPage : LoginPage
      }
      exact
    />
    <Route
      path='/policies'
      component={
        user.Role === 0 || user.Role === 1 || user.Role === 2
          ? PoliciesPage
          : LoginPage
      }
      exact
    />
    {/* 404 */}
    <Route component={NotFoundPage} exact />
  </Switch>
);

export default Routes;
