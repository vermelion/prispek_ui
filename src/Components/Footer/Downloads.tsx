import React from 'react';
import Download from './Download';

const Downloads = () => (
  <div className='footer__downloads'>
    <Download
      title='Downloads:'
      links={[
        {
          href:
            'prispek.co.za/wp-content/uploads/2020/04/Client_Consent_Form-Cells.pdf',
          name: 'Client Consent Form'
        },
        {
          href:
            'http://prispek.co.za/wp-content/uploads/2020/04/SPEKTRA_PROSPEKTUS_Apr2020.pdf',
          name: 'Spektra Prospektus (Afrikaans)'
        },
        {
          href:
            'http://prispek.co.za/wp-content/uploads/2020/04/SPEKTRA_PROSPECTUS.pdf',
          name: 'Spektra Prospektus (English)'
        },
        {
          href: 'http://prispek.co.za/faq/guides/',
          name: 'FAQ'
        },
        {
          href:
            'http://prispek.co.za/wp-content/uploads/2020/06/COVID-Workplace-Policy-31May2020.pdf',
          name: 'COVID-Workplace-Policy'
        }
      ]}
    />
    <Download
      title='Important Downloads:'
      links={[
        {
          href:
            'http://prispek.co.za/wp-content/uploads/2020/04/SPEKTRA_PROSPEKTUS_Apr2020.pdf',
          name: 'Spektra Bekendstellings Dokument'
        },
        {
          href:
            'http://prispek.co.za/wp-content/uploads/2020/04/SPEKTRA_PROSPECTUS.pdf',
          name: 'Spektra Disclosure Document'
        }
      ]}
    />
  </div>
);

export default Downloads;
