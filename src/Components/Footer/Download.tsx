import { DownloadType } from '../../Config/Typescript/DownloadType';

const Download = ({ title, links }: DownloadType) => (
  <ul className='footer__downloads__list'>
    <li className='footer__downloads__list__item'>
      <h4 className='footer__downloads__list__item__title'>{title}</h4>
    </li>
    {links.map((link: any) => (
      <div key={link.href}>
        <li className='footer__downloads__list__item'>
          <a
            target='_blank'
            rel='noopener noreferrer'
            href={link.href}
            className='footer__downloads__list__item__link'
          >
            {link.name}
          </a>
        </li>
      </div>
    ))}
  </ul>
);

export default Download;
