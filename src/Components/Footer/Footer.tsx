import React from 'react';

import '../../Assets/sass/footer.scss';
import { useTheme } from '../../Config/Theme/Theme';
import SAIPA from '../../Assets/images/SAIPA.png';
import PRISPEK from '../../Assets/images/PRISPEK.png';
import PRISMA from '../../Assets/images/PRISMA.jpg';
import SPEKTRA from '../../Assets/images/SPEKTRA.png';
import VERMELION from '../../Assets/images/VERMELION.png';
import Contacts from './Contacts';
import Downloads from './Downloads';

const Footer = () => {
  const { grey } = useTheme();
  return (
    <div id='footer' className='footer' style={{ background: grey }}>
      <div className='footer--gridArea'>
        <Contacts />
        <Downloads />
        <div className='footer__download__disclaimer'>
          <p>
            You are welcome to download any guide that could assist you in
            making more informed decisions and be better prepared for those
            events.
          </p>
          <p>
            Please note that these guides does not constitute advice, legal or
            ortherwise, but should be used in conjunction with adivce received
            from your accountant, financial advisor and/or attorney following a
            proper analysis of your individual situation and needs.
          </p>
        </div>
      </div>
      <div className='footer--grid'>
        <div className='footer__logo'>
          <p className='footer__logo__caption' />
          <a href='/'>
            <img
              src={PRISPEK}
              alt='Prispek Logo'
              className='footer__logo__image'
            />
          </a>
        </div>
        <div className='footer__logo'>
          <p className='footer__logo__caption' />
          <a
            href='https://www.vermelion.net'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img
              src={SPEKTRA}
              alt='Vermelion Logo'
              className='footer__logo__image'
            />
          </a>
        </div>
        <div className='footer__logo'>
          <p className='footer__logo__caption' />
          <a
            href='https://www.vermelion.net'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img
              src={PRISMA}
              alt='Vermelion Logo'
              className='footer__logo__image'
            />
          </a>
        </div>
        <div className='footer__logo'>
          <p className='footer__logo__caption'>Prisma is a member of:</p>
          <a
            href='https://www.saipa.co.za'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img src={SAIPA} alt='Saipa Logo' className='footer__logo__image' />
          </a>
        </div>
        <div className='footer__logo'>
          <p className='footer__logo__caption' />
          <a
            href='https://www.vermelion.net'
            target='_blank'
            rel='noopener noreferrer'
          >
            <img
              src={VERMELION}
              alt='Vermelion Logo'
              className='footer__logo__image'
            />
          </a>
        </div>
      </div>

      <p className='footer__copyright'>
        &copy; Prispek {new Date().getFullYear()} | Developed by Vermelion
      </p>
    </div>
  );
};

export default Footer;
