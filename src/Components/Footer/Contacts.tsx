import React from 'react';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PhoneInTalkOutlinedIcon from '@material-ui/icons/PhoneInTalkOutlined';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

const Contacts = ({ page }: { page?: boolean }) => (
  <div className='footer__contact'>
    <ul className='footer__contact__list'>
      <h4 className='footer__contact__title'>Contact Us:</h4>
      <li className='footer__contact__list__item'>
        <a
          href='https://www.google.com/maps/place/Prispek+Consulting/@-25.6873846,28.231015,15z/data=!4m5!3m4!1s0x0:0x6d737c2de80ddf05!8m2!3d-25.6868226!4d28.2322884'
          className='footer__contact__list__item__link'
          target='_blank'
          rel='noopener noreferrer'
        >
          Kruin Business Centre <br /> 406 Braam Pretorius (Corner of Gryshout
          Ave)
          <br /> Magalieskruin
          <br /> 0182
        </a>
      </li>
      <li className='footer__contact__list__item'>
        <a href='/'>P.O. Box 3167, Montana Park, 0159</a>
      </li>
      <li className='footer__contact__list__item'>
        <a href='/' className='footer__contact__list__item__link'>
          <MailOutlineIcon className='footer__contact__list__item__link__icon' />{' '}
          service@prispek.co.za
        </a>
      </li>
      <li className='footer__contact__list__item'>
        <a href='/' className='footer__contact__list__item__link'>
          <PhoneInTalkOutlinedIcon className='footer__contact__list__item__link__icon' />{' '}
          +27 12 567 5502
        </a>
      </li>
      {!page && (
        <li className='footer__contact__list__item'>
          <a href='/' className='footer__contact__list__item__link'>
            <ErrorOutlineIcon className='footer__contact__list__item__link__icon' />{' '}
            Disclaimers
          </a>
        </li>
      )}
    </ul>
  </div>
);

export default Contacts;
