import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import LinkIcon from '@material-ui/icons/Link';
import { Tooltip } from '@material-ui/core';
import { BlogPostType } from '../../Config/Typescript/BlogPostType';

const BlogPost = ({
  title,
  date,
  media,
  description,
  link,
  href
}: BlogPostType) => {
  return (
    <Card>
      {/* <img
        src='https://external.fjnb7-1.fna.fbcdn.net/safe_image.php?d=AQHdTRQZl_vinwX7&w=500&h=261&url=https%3A%2F%2Fgallery.mailchimp.com%2Ff3054cb4c9a03dd8b9b8f5349%2Fimages%2F594e80ce-bf5c-4074-8369-4ff9f809c922.png&cfs=1&ext=jpg&_nc_cb=1&_nc_hash=AQHlpg2jLaCf601d'
        alt=''
      /> */}
      <CardHeader
        avatar={<Avatar aria-label='recipe'>R</Avatar>}
        title={title}
        subheader={date}
      />
      <CardMedia
        style={{
          height: 0,
          paddingTop: '40%'
        }}
        image={media}
        title='Paella dish'
      />
      <CardContent>
        <Typography variant='body2' color='textSecondary' component='p'>
          {description}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Tooltip title={link}>
          <IconButton aria-label='Post Link' href={href} target='_blank'>
            <LinkIcon />
          </IconButton>
        </Tooltip>
      </CardActions>
    </Card>
  );
};

export default BlogPost;
