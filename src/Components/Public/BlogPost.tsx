import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import LinkIcon from '@material-ui/icons/Link';
import { Tooltip } from '@material-ui/core';
import { BlogPostType } from '../../Config/Typescript/BlogPostType';

const BlogPost = ({
  direction,
  title,
  date,
  media,
  alt,
  description,
  link,
  href
}: BlogPostType) => {
  return (
    <div className={`blog__post ${direction}`}>
      <div className='blog__post__header'>
        <a
          target='_blank'
          rel='noopener noreferrer'
          href='https://www.facebook.com/prispek/?__cft__[0]=AZXSkPvyFidepsOXZy5IMZCpsR3rOAZk8H4LNMkRNJ-UYmSj0ghB_uMU-o5iqIbZap4fe4rII_1-vR1LZPLp4XdleJ4121NkpGiwMeCslJjd3F35M_AaVxA4vVSmcSi6NXPtdRPu1COEnN6me4oFOuvM&__tn__=%3C%3C%2CP-R'
          className='blog__post__header__avatar'
        >
          <img
            src='https://scontent.fjnb7-1.fna.fbcdn.net/v/t31.0-8/23783793_1875810722730491_8524253810601560108_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_ohc=cOzrmjlQ74gAX88svcc&_nc_ht=scontent.fjnb7-1.fna&oh=2424d8e80558884d86898d18407464d8&oe=601BD5D0'
            alt='avatar'
            className='blog__post__header__avatar__image'
          />
        </a>
        <h2 className='blog__post__header__title'>{title}</h2>
        <p className='blog__post__header__date'>{date}</p>
      </div>
      <div className='blog__post__media'>
        <img src={media} alt={alt} className='blog__post__media__image' />
      </div>
      <div className='blog__post__content'>
        <p className='blog__post__content__description'>{description}</p>
        <Tooltip title={link}>
          <IconButton
            aria-label='Post Link'
            size='small'
            href={href}
            target='_blank'
          >
            <LinkIcon />
          </IconButton>
        </Tooltip>
      </div>
    </div>
  );
};

export default BlogPost;
