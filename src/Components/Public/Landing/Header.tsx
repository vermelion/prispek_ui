import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import '../../../Assets/sass/landing/header.scss';
import Background from '../../../Assets/videos/heading_background.mp4';

const Header = () => (
  <div className='header'>
    <video className='header__background' autoPlay loop muted>
      <source src={Background} type='video/mp4' />
    </video>
    <div
      className='section'
      style={{
        background: 'transparent',
        display: 'grid',
        placeContent: 'center'
      }}
    >
      <h1 className='header__title'>Prispek Consulting</h1>
      <h4 className='header__slogan'>
        Wealth Creation With Integrity since 1996
      </h4>
      <div className='header__cta--group'>
        <Link to='/contact'>
          <Button variant='contained' color='primary' className='header__cta'>
            Book Appointment
          </Button>
        </Link>
        <Button
          href='#footer'
          variant='contained'
          color='secondary'
          style={{ color: 'white' }}
        >
          Get in Touch
        </Button>
      </div>
    </div>
  </div>
);

export default Header;
