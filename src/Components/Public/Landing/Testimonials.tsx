import '../../../Assets/sass/landing/testimonials.scss';
import avatar from '../../../Assets/images/Avatar.png';

const Testimonials = () => (
  <div className='testimonial'>
    <img src={avatar} className='testimonial__avatar' alt='avatar' />
    <div className='testimonial__description'>
      <h2 className='testimonial__description__author'>Arie Hoogenboezem,</h2>
      <h3 className='testimonial__description__author--description'>
        MD, Q-Mech Consulting Engineers
      </h3>
      <p className='testimonial__description__paragraph'>
        “There are few one-stop financial service centres that really deliver
        eminently skilled services as does Prispek Consulting. I have complete
        confidence in the ability of the accountants at Prisma to keep the
        bookkeeping, payroll and tax administration of my business in order,
        thus enabling me to focus on the speciality which I practice without
        being bothered with financial administration. Spektra Brokers offers me
        so much more than the general long-term insurance broker / investment
        advisor. I have full confidence on entrusting my long-term insurance and
        investment portfolio to them. Should an unforseen event incapacitate me
        or lead to my demise, I can rest assured that my estate and execution
        therof will be handled professionally so as to provide for my next of
        kin in the manner that I myself would have preferred.”
      </p>
    </div>
  </div>
);

export default Testimonials;
