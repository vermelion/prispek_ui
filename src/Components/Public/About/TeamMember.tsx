const TeamMember = ({
  src,
  alt,
  flex
}: {
  src: string;
  alt: string;
  flex?: boolean;
}) => (
  <div
    className='about__team__grid__member'
    style={{ marginRight: flex ? '2.5rem' : 0 }}
  >
    <img src={src} alt={alt} className='about__team__grid__member__image' />
  </div>
);

export default TeamMember;
