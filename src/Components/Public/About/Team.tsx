import React from 'react';
import TeamMember from './TeamMember';
import JohnDoe from '../../../Assets/images/JohnDoe.jpg';
import Button from '@material-ui/core/Button';

const Team = () => (
  <div className='about__team'>
    <h1 className='about__team__heading'>Team</h1>
    <h2 className='about__team__title'>Founders</h2>
    <div className='about__team__flex'>
      <TeamMember src={JohnDoe} alt='test' flex />
      <TeamMember src={JohnDoe} alt='test' />
    </div>
    <h2 className='about__team__title'>Employees</h2>
    <div className='about__team__grid'>
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
      <TeamMember src={JohnDoe} alt='test' />
    </div>
    <Button variant='contained' color='primary' fullWidth={false}>
      Book Appointment
    </Button>
  </div>
);

export default Team;
