/* eslint-disable no-empty-pattern */
import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Button from '@material-ui/core/Button';

import { AboutCardType } from '../../../Config/Typescript/AboutCardType';
import { Link } from 'react-router-dom';
import { useEvents } from '../../../Config/Events/EventsContext';
// import { useConfig } from '../../../Config/ConfigContex';

const AboutCard = ({
  title,
  logo,
  about,
  readMore,
  list,
  footer
}: AboutCardType) => {
  // const [{}, dispatch] = useConfig();
  const { changeAboutLoad, reset } = useEvents();
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      media: {
        paddingTop:
          title === 'Prispek' ? '30%' : title === 'Spektra' ? '25%' : '40%',
        width: '100%',
        margin: 'auto',
        overflow: 'show'
      },
      expand: {
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest
        })
      }
    })
  );
  const classes = useStyles();

  return (
    <Card className={`about__card ${readMore && 'readMore'}`} id={title}>
      <CardHeader title={title} subheader='History' />
      <CardMedia className={classes.media} image={logo} />
      <CardContent>
        {about.map(about => (
          <Typography key={about} paragraph style={{ marginBottom: '0.5rem' }}>
            {about}
          </Typography>
        ))}
        {list && (
          <List
            component='nav'
            aria-labelledby='nested-list-subheader'
            subheader={
              <ListSubheader component='div' id='nested-list-subheader'>
                <Typography color='textPrimary' variant='h6'>
                  {list.title}
                </Typography>
              </ListSubheader>
            }
          >
            {list.items.map((item: any) => (
              <ListItem key={item.key}>
                <ListItemIcon>
                  <FiberManualRecordIcon style={{ color: '#000' }} />
                </ListItemIcon>
                <ListItemText primary={item.name} />
              </ListItem>
            ))}
          </List>
        )}
        {footer && <Typography paragraph>{footer}</Typography>}
      </CardContent>

      <CardActions
        disableSpacing
        style={{ justifySelf: 'flex-end', alignSelf: 'flex-end' }}
      >
        {readMore && (
          <Link
            to={`/about#${title}`}
            style={{
              marginLeft: 'auto',
              marginTop: 'auto',
              marginRight: '1rem'
            }}
          >
            <Button
              variant='outlined'
              aria-label='book appointment'
              onClick={() => changeAboutLoad(title)}
            >
              Read More
            </Button>
          </Link>
        )}
        <Link
          to='/services'
          style={{ marginLeft: readMore ? 0 : 'auto', marginRight: '1rem' }}
        >
          <Button
            variant='outlined'
            aria-label='services'
            onClick={() => reset()}
          >
            Services
          </Button>
        </Link>
        <Link to='/contact'>
          <Button
            variant='outlined'
            aria-label='book appointment'
            onClick={() => reset()}
          >
            Book Appointment
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default AboutCard;
