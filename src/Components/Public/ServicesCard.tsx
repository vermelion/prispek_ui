import React from 'react';

import Button from '@material-ui/core/Button';
import { ServiceCardType } from '../../Config/Typescript/ServiceCard';

const ServicesCard = ({
  invert,
  icon,
  alt,
  title,
  cta,
  href,
  area
}: ServiceCardType) => (
  <div className={`services--cards__card ${invert && 'invert'} ${area}`}>
    <div className='services--cards__card__left'>
      <img src={icon} alt={alt} className='services--cards__card__left__icon' />
    </div>
    <div className='services--cards__card__right'>
      {title && (
        <h1 className='services--cards__card__right__title'>{title}</h1>
      )}
      {cta && (
        // eslint-disable-next-line react/jsx-no-target-blank
        <a href={href} target='_blank'>
          <Button
            variant='contained'
            color='primary'
            size='large'
            className='services--cards__card__right__cta'
          >
            {cta}
          </Button>
        </a>
      )}
    </div>
  </div>
);

export default ServicesCard;
