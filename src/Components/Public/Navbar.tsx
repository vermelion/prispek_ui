/* eslint-disable no-empty-pattern */
import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import HomeIcon from '@material-ui/icons/Home';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import CallIcon from '@material-ui/icons/Call';
import EventNoteIcon from '@material-ui/icons/EventNote';
import InfoIcon from '@material-ui/icons/Info';
import { useTheme } from '../../Config/Theme/Theme';
import { Button } from '@material-ui/core';

import '../../Assets/sass/navbar.scss';
import PRISPEK from '../../Assets/images/PRISPEK.png';
import { NavbarType } from '../../Config/Typescript/NavbarType';
import { useEvents } from '../../Config/Events/EventsContext';

const Navbar = ({
  landing,
  about,
  services,
  blog,
  contact,
  login
}: NavbarType) => {
  const { lightYellow } = useTheme();
  const { reset } = useEvents();
  return (
    <>
      <AppBar position='fixed' className='navbar' elevation={0}>
        <Toolbar>
          <Link to='/' className='navbar__logo'>
            <img src={PRISPEK} alt='Prispek Logo' />
          </Link>
        </Toolbar>
        <Toolbar>
          <Link to='/'>
            <Button
              onClick={() => reset()}
              variant='text'
              className='navbar__link'
              color={landing ? 'primary' : 'default'}
              startIcon={<HomeIcon />}
            >
              Home
            </Button>
          </Link>
          <Link to='/about'>
            <Button
              onClick={() => reset()}
              variant='text'
              className='navbar__link'
              color={about ? 'primary' : 'default'}
              startIcon={<InfoIcon />}
            >
              About
            </Button>
          </Link>
          <Link to='/services'>
            <Button
              onClick={() => reset()}
              variant='text'
              className='navbar__link'
              color={services ? 'primary' : 'default'}
              startIcon={<EventNoteIcon />}
            >
              Services
            </Button>
          </Link>
          <Link to='/blog'>
            <Button
              onClick={() => reset()}
              variant='text'
              className='navbar__link'
              color={blog ? 'primary' : 'default'}
              startIcon={<EventNoteIcon />}
            >
              Blogs
            </Button>
          </Link>
          <Link to='/contact'>
            <Button
              onClick={() => reset()}
              className='navbar__link'
              color={contact ? 'primary' : 'default'}
              startIcon={<CallIcon />}
            >
              Contact
            </Button>
          </Link>
          <Link to='/login'>
            <Tooltip title='Staff Login' arrow>
              <IconButton
                className='navbar__link key'
                style={{
                  background: lightYellow,
                  color: 'black',
                  opacity: login ? 0.5 : 1
                }}
              >
                <VpnKeyIcon />
              </IconButton>
            </Tooltip>
          </Link>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Navbar;
