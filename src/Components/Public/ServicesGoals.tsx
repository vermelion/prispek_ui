import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Paper from '@material-ui/core/Paper';

const ServicesGoals = () => (
  <Paper square elevation={0} className='services--about__goals'>
    <Typography className='services--about__goals__title'>
      Prispek strives to:
    </Typography>
    <List aria-label='Prispek Goals List'>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              benchmark
              <strong> Integerity above all.</strong>
            </p>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              offer a <strong>comprehensive professional</strong> financial
              service to you.
            </p>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              make available to our clients
              <strong> highly competent specialists</strong> who are up-to-date
              with the latest trends affecting the fast-changing financial
              world.
            </p>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              improve the <strong>profitability</strong> of our clients’ unique
              enterprises by applying our services.
            </p>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              improve the ongoing <strong>effectiveness</strong> of enterprises
              through the implementation of relevant technologies.
            </p>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              ensure peace of mind by the application and maintenance of all
              relevant <strong>legislation</strong> according to Christian norms
              and standards.
            </p>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <FiberManualRecordIcon />
        </ListItemIcon>
        <ListItemText
          primary={
            <p>
              provide regular <strong>updates</strong> on your financial plan
              &amp; tax affairs to ensure that you{' '}
              <strong>reach your financial goals.</strong>
            </p>
          }
        />
      </ListItem>
    </List>
  </Paper>
);

export default ServicesGoals;
