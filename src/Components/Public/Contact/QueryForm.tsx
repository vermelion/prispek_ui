import React from 'react';
import Button from '@material-ui/core/Button';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { TextField } from 'formik-material-ui';
import Grid from '@material-ui/core/Grid';

const QueryForm = () => {
  //? Intitial Values
  const initialValues = {
    name: '',
    email: '',
    number: '',
    message: ''
  };
  //? Validation
  const validationSchema = Yup.object({
    name: Yup.string().required('Required'),
    email: Yup.string().email('Invalid Email').required('Required'),
    message: Yup.string().required('Required')
  });
  //? Handle Submit
  const contactSubmit = (values: object) => {
    console.log('Values: ', values);
  };
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={contactSubmit}
    >
      {({ values }) => (
        <Form className='contact--book__form'>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='name'
                type='text'
                label='Name:'
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='email'
                type='email'
                label='Email:'
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='number'
                type='text'
                label='Phone:'
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='message'
                type='text'
                label='Message:'
                rows={3}
                multiline
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                type='submit'
                variant='contained'
                color='primary'
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default QueryForm;
