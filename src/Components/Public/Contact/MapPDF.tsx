import pdf from '../../../Assets/pdfs/map.pdf';

const MapPDF = () => (
  <iframe
    src={pdf}
    title='Map_PDF'
    width='80%'
    height='600px'
    style={{ margin: '0 auto' }}
  ></iframe>
);

export default MapPDF;
