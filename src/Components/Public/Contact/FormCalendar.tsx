import React from 'react';
import Paper from '@material-ui/core/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  WeekView,
  MonthView,
  Toolbar,
  ViewSwitcher,
  AppointmentTooltip,
  Appointments
} from '@devexpress/dx-react-scheduler-material-ui';
import { appointments } from './appointments';

const FormCalendar = () => {
  // let date = new Date();
  // let yyyy = date.getFullYear();
  // let mm = date.getMonth();
  // let dd = date.getDay();
  // let currentDate = new Date(yyyy - mm - dd);
  return (
    <Paper
      style={{
        width: '100%',
        margin: 'auto',
        border: 'none'
      }}
    >
      <Scheduler data={appointments} height={600}>
        <ViewState
          defaultCurrentDate={'2018-07-25'}
          defaultCurrentViewName='Week'
        />
        <WeekView excludedDays={[0, 6]} startDayHour={9} endDayHour={19} />
        <MonthView />
        <Appointments />
        <AppointmentTooltip />
        <Toolbar />
        <ViewSwitcher />
      </Scheduler>
    </Paper>
  );
};

export default FormCalendar;
