import React from 'react';
import Button from '@material-ui/core/Button';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { TextField } from 'formik-material-ui';
import Grid from '@material-ui/core/Grid';
import { MenuItem } from '@material-ui/core';

const BookForm = () => {
  const admins = ['Christo', 'Gerhard', 'Both'];
  //? Intitial Values
  const initialValues = {
    name: '',
    email: '',
    number: '',
    message: '',
    date: '',
    select: ''
  };
  //? Validation
  const validationSchema = Yup.object({
    name: Yup.string().required('Required'),
    email: Yup.string().email('Invalid Email').required('Required'),
    date: Yup.string().required('Required'),
    select: Yup.string().required('Required')
  });
  //? Handle Submit
  const contactSubmit = (values: object) => {
    console.log('Values: ', values);
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={contactSubmit}
    >
      {({ values }) => (
        <Form className='contact--book__form'>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='name'
                type='text'
                label='Name:'
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='email'
                type='email'
                label='Email:'
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='number'
                type='text'
                label='Phone:'
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='message'
                type='text'
                label='Notes:'
                rows={3}
                multiline
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='date'
                label='Appointment Date:'
                type='datetime-local'
                InputLabelProps={{
                  shrink: true
                }}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                name='select'
                label='Appointment select:'
                select
                fullWidth
              >
                {admins.map(option => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </Field>
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                type='submit'
                variant='contained'
                color='primary'
              >
                Book
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default BookForm;
