import React from 'react';
import { Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { TextField } from 'formik-material-ui';
import { useConfig } from '../../Config/ConfigContex';

const LoginForm = () => {
  const alert = useAlert();
  const [{ route, user }, dispatch] = useConfig();
  //? Intitial Values
  const initialValues = {
    username: '',
    password: ''
  };
  //? Validation
  const validationSchema = Yup.object({
    username: Yup.string().email('Invalid Email').required('Required'),
    password: Yup.string().required('Required')
  });
  //? Handle Submit
  const contactSubmit = (values: { username: string; password: string }) => {
    axios
      .post(`${route}/utLogin`, {
        email: values.username,
        password: values.password
      })
      .then((res: any) => {
        console.log(res);

        if (res.data.result === 'error') {
          alert.error(res.data.frontEndMessage);
        } else {
          setTimeout(() => {
            dispatch({ type: 'SET_USER', user: res.data.data });
          }, 500);
        }
      });
  };
  return (
    <>
      {(user.Role === 1 || user.Role === 0) && <Redirect to='/view-members' />}
      {user.Role === 2 && <Redirect to='/policies' />}
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={contactSubmit}
      >
        {({ values }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  color='primary'
                  name='username'
                  type='email'
                  label='Username:'
                  required
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Field
                  component={TextField}
                  color='primary'
                  name='password'
                  type='password'
                  label='Password:'
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  fullWidth
                  type='submit'
                  variant='contained'
                  color='primary'
                >
                  Login
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default LoginForm;
