import React, { useState } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import ViewMembersTableData from './VewMembersTableData';
import { ViewMembersTableType } from '../../Config/Typescript/ViewMembersTableType';
import ViewMembersTableHead from './ViewMembersTableHead';

function a11yProps(index: number) {
  return {
    id: `view-members-tab-${index}`,
    'aria-controls': `view-members-tabpanel-${index}`
  };
}
const ViewMembersTable = ({
  getMembers,
  setGetMembers,
  response,
  primary,
  employees,
  admin,
  viewMember,
  dispatch
}: ViewMembersTableType) => {
  const [page, setPage] = useState(0);
  const [membersPerPage, setMembersPerPage] = useState(5);
  const [sortBy, setSortBy] = useState('Name');
  const [sortState, setSortState] = useState(false);
  const [selectedMember, setSelectedMember] = useState<string | null>(null);

  const handleChange = (_event: React.ChangeEvent<{}>, newValue: number) => {
    dispatch({ type: 'SET_VIEW-MEMBER', viewMember: newValue });
    setPage(0);
  };

  //? Pagination

  const handleChangePage = (_event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeMembersPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setMembersPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const emptyMembers =
    membersPerPage -
    Math.min(
      membersPerPage,
      viewMember === 0 ? employees.length : admin.length - page * membersPerPage
    );

  return (
    <TableContainer component={Paper}>
      <AppBar position='static'>
        <Tabs
          value={viewMember}
          onChange={handleChange}
          aria-label='View Members Tab'
        >
          <Tab label='Employees' {...a11yProps(0)} />
          <Tab label='Admin' {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <Table size='small' aria-label='Members table'>
        <ViewMembersTableHead
          sortBy={sortBy}
          setSortBy={setSortBy}
          sortState={sortState}
          setSortState={setSortState}
          primary={primary}
        />
        {/*  */}
        <TableBody>
          {response ? (
            viewMember === 0 ? (
              employees
                .sort(function (a: any, b: any) {
                  let item1 =
                    sortBy === 'Name'
                      ? a.Name.toLowerCase()
                      : sortBy === 'Surname'
                      ? a.Surname.toLowerCase()
                      : sortBy === 'Email'
                      ? a.Email.toLowerCase()
                      : a.Active;

                  let item2 =
                    sortBy === 'Name'
                      ? b.Name.toLowerCase()
                      : sortBy === 'Surname'
                      ? b.Surname.toLowerCase()
                      : sortBy === 'Email'
                      ? b.Email.toLowerCase()
                      : b.Active;
                  if (sortState) {
                    if (item1 < item2) {
                      return 1;
                    }
                    if (item1 > item2) {
                      return -1;
                    }
                  } else {
                    if (item1 < item2) {
                      return -1;
                    }
                    if (item1 > item2) {
                      return 1;
                    }
                  }
                  return 0;
                })
                .slice(
                  page * membersPerPage,
                  page * membersPerPage + membersPerPage
                )
                .map((member: any) => (
                  <ViewMembersTableData
                    key={member.Guid}
                    getMembers={getMembers}
                    setGetMembers={setGetMembers}
                    selectedMember={selectedMember}
                    setSelectedMember={setSelectedMember}
                    memberRole={2}
                    member={member}
                  />
                ))
            ) : (
              admin
                .sort(function (a: any, b: any) {
                  let item1 =
                    sortBy === 'Name'
                      ? a.Name.toLowerCase()
                      : sortBy === 'Surname'
                      ? a.Surname.toLowerCase()
                      : sortBy === 'Email'
                      ? a.Email.toLowerCase()
                      : a.Active;

                  let item2 =
                    sortBy === 'Name'
                      ? b.Name.toLowerCase()
                      : sortBy === 'Surname'
                      ? b.Surname.toLowerCase()
                      : sortBy === 'Email'
                      ? b.Email.toLowerCase()
                      : b.Active;
                  if (sortState) {
                    if (item1 < item2) {
                      return 1;
                    }
                    if (item1 > item2) {
                      return -1;
                    }
                  } else {
                    if (item1 < item2) {
                      return -1;
                    }
                    if (item1 > item2) {
                      return 1;
                    }
                  }
                  return 0;
                })
                .slice(
                  page * membersPerPage,
                  page * membersPerPage + membersPerPage
                )
                .map((member: any) => (
                  <ViewMembersTableData
                    key={member.Guid}
                    getMembers={getMembers}
                    setGetMembers={setGetMembers}
                    selectedMember={selectedMember}
                    setSelectedMember={setSelectedMember}
                    memberRole={1}
                    member={member}
                  />
                ))
            )
          ) : (
            <CircularProgress color='secondary' />
          )}
          {emptyMembers > 0 && (
            <TableRow style={{ height: 53 * emptyMembers }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              count={viewMember === 0 ? employees.length : admin.length}
              rowsPerPage={membersPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeMembersPerPage}
            />
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ViewMembersTable;
