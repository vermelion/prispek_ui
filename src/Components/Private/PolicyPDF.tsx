const PolicyPDF = ({ pdf, title }: { pdf: string; title: string }) => (
  <iframe
    src={pdf}
    title={title}
    height='550px'
    style={{ width: '100%' }}
  ></iframe>
);

export default PolicyPDF;
