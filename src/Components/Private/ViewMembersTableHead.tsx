import React from 'react';
import Typography from '@material-ui/core/Typography';
import TableHead from '@material-ui/core/TableHead';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import IconButton from '@material-ui/core/IconButton';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { ViewMembersTableHeadType } from '../../Config/Typescript/ViewMembersTableType';

const ViewMembersTableHead = ({
  sortBy,
  setSortBy,
  sortState,
  setSortState,
  primary
}: ViewMembersTableHeadType) => (
  <TableHead>
    <TableRow>
      <TableCell>
        <Typography variant='caption'>
          Name
          <IconButton
            size='small'
            onClick={() => {
              sortBy === 'Name'
                ? setSortState(!sortState)
                : setSortState(false);
              setSortBy('Name');
            }}
            style={{
              marginLeft: '0.5rem',
              color: sortBy === 'Name' && primary,
              transform:
                sortBy === 'Name' && sortState
                  ? 'rotate(-90deg)'
                  : 'rotate(90deg)'
            }}
          >
            {sortBy === 'Name' && sortState ? (
              <ArrowRightAltIcon className='upArrow' />
            ) : (
              <ArrowRightAltIcon className='downArrow' />
            )}
          </IconButton>
        </Typography>
      </TableCell>
      <TableCell>
        <Typography variant='caption'>
          Surname
          <IconButton
            size='small'
            onClick={() => {
              sortBy === 'Surname'
                ? setSortState(!sortState)
                : setSortState(false);
              setSortBy('Surname');
            }}
            style={{
              marginLeft: '0.5rem',
              color: sortBy === 'Surname' && primary,
              transform:
                sortBy === 'Surname' && sortState
                  ? 'rotate(-90deg)'
                  : 'rotate(90deg)'
            }}
          >
            {sortBy === 'Surname' && sortState ? (
              <ArrowRightAltIcon className='upArrow' />
            ) : (
              <ArrowRightAltIcon className='downArrow' />
            )}
          </IconButton>
        </Typography>
      </TableCell>
      <TableCell>
        <Typography variant='caption'>
          Email
          <IconButton
            size='small'
            onClick={() => {
              sortBy === 'Email'
                ? setSortState(!sortState)
                : setSortState(false);
              setSortBy('Email');
            }}
            style={{
              marginLeft: '0.5rem',
              color: sortBy === 'Email' && primary,
              transform:
                sortBy === 'Email' && sortState
                  ? 'rotate(-90deg)'
                  : 'rotate(90deg)'
            }}
          >
            {sortBy === 'Email' && sortState ? (
              <ArrowRightAltIcon className='upArrow' />
            ) : (
              <ArrowRightAltIcon className='downArrow' />
            )}
          </IconButton>
        </Typography>
      </TableCell>
      <TableCell>
        <Typography variant='caption'>
          Active
          <IconButton
            size='small'
            onClick={() => {
              sortBy === 'Active'
                ? setSortState(!sortState)
                : setSortState(false);
              setSortBy('Active');
            }}
            style={{
              marginLeft: '0.5rem',
              color: sortBy === 'Active' && primary,
              transform:
                sortBy === 'Active' && sortState
                  ? 'rotate(-90deg)'
                  : 'rotate(90deg)'
            }}
          >
            {sortBy === 'Active' && sortState ? (
              <ArrowRightAltIcon className='upArrow' />
            ) : (
              <ArrowRightAltIcon className='downArrow' />
            )}
          </IconButton>
        </Typography>
      </TableCell>
      <TableCell padding='checkbox' />
    </TableRow>
  </TableHead>
);

export default ViewMembersTableHead;
