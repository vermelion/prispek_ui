import React from 'react';
import { useAlert } from 'react-alert';
import { Link } from 'react-router-dom';
import axios from 'axios';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import EventNoteIcon from '@material-ui/icons/EventNote';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Button from '@material-ui/core/Button';

import '../../Assets/sass/navbar.scss';
import { NavbarType } from '../../Config/Typescript/NavbarType';
import { useTheme } from '../../Config/Theme/Theme';
import { useConfig } from '../../Config/ConfigContex';
import PRISPEK from '../../Assets/images/PRISPEK.png';

const AdminNavbar = ({ view, policies }: NavbarType) => {
  const alert = useAlert();
  const { lightYellow } = useTheme();
  const [{ route, user }, dispatch] = useConfig();
  const logout = () => {
    axios
      .post(`${route}/utLogoff`, {
        member_guid: user.MemberGuid,
        session_guid: user.SessionGuid
      })
      .then(res => {
        console.log(res);
        if (res.data.result === 'success') {
          alert.success('Logged Out!');
          dispatch({ type: 'RESET' });
        } else {
          alert.error(res.data.frontEndMessage);
          dispatch({ type: 'RESET' });
        }
      })
      .catch(err => console.log(err));
  };
  return (
    <>
      <AppBar position='fixed' className='navbar' elevation={0}>
        <Toolbar>
          <Link to='/' className='navbar__logo'>
            <img src={PRISPEK} alt='Prispek Logo' />
          </Link>
        </Toolbar>
        <Toolbar>
          {(user.Role === 0 || user.Role === 1) && (
            <Link to='/view-members'>
              <Button
                variant='text'
                className='navbar__link'
                color={view ? 'primary' : 'default'}
                startIcon={<EventNoteIcon />}
              >
                View Members
              </Button>
            </Link>
          )}
          <Link to='/policies'>
            <Button
              variant='text'
              className='navbar__link'
              color={policies ? 'primary' : 'default'}
              startIcon={<EventNoteIcon />}
            >
              Policies
            </Button>
          </Link>
          <Link to='/login'>
            <Tooltip title='Logout' arrow>
              <IconButton
                className='navbar__link key'
                style={{
                  background: lightYellow,
                  color: 'black'
                }}
                onClick={logout}
              >
                <ExitToAppIcon />
              </IconButton>
            </Tooltip>
          </Link>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default AdminNavbar;
