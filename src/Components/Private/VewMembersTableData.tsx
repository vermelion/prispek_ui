import React, { useState } from 'react';
import axios from 'axios';
import { useAlert } from 'react-alert';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Collapse from '@material-ui/core/Collapse';
import MenuItem from '@material-ui/core/MenuItem';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import { makeStyles } from '@material-ui/core';
import { useConfig } from '../../Config/ConfigContex';
import { ViewMembersTableDataType } from '../../Config/Typescript/ViewMembersTableType';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset'
    }
  },
  hideBodrder: {
    borderBottom: 'unset'
  }
});

const ViewMembersTableData = ({
  getMembers,
  setGetMembers,
  selectedMember,
  setSelectedMember,
  memberRole,
  member
}: ViewMembersTableDataType) => {
  const alert = useAlert();
  const [{ route, user }] = useConfig();
  const classes = useRowStyles();
  const [open, setOpen] = useState(false);
  const [name, setName] = useState(member.Name);
  const [surname, setSurname] = useState(member.Surname);
  const [email, setEmail] = useState(member.Email);
  const [role, setRole] = useState<number>(memberRole);

  const edit = () => {
    setOpen(true);
    setSelectedMember(member.Guid);
  };

  const save = () => {
    if (
      name !== member.Name ||
      surname !== member.Surname ||
      email !== member.Email
    ) {
      axios
        .put(`${route}/dbAdminEditMember`, {
          member_guid: user.MemberGuid,
          session_guid: user.SessionGuid,
          data: {
            Name: name,
            Surname: surname,
            Email: email,
            MemberGuid: member.Guid
          }
        })
        .then(res => {
          console.log(res);
          if (res.data.result === 'success') {
            setGetMembers(!getMembers);
            alert.success(`Edited ${member.Name} ${member.Surname}`);
            setOpen(false);
            setSelectedMember(null);
          } else {
            alert.error(res.data.frontEndMessage);
          }
        });
    } else if (role !== memberRole) {
      axios
        .put(`${route}/dbChangeMemberRole`, {
          member_guid: user.MemberGuid,
          session_guid: user.SessionGuid,
          data: {
            Role: role,
            MemberGuid: member.Guid
          }
        })
        .then(res => {
          console.log(res);
          if (res.data.result === 'success') {
            setGetMembers(!getMembers);
            alert.success(`Changed ${member.Name} ${member.Surname} Role`);
            setOpen(false);
            setSelectedMember(null);
          } else {
            alert.error(res.data.frontEndMessage);
          }
        });
    } else {
      setOpen(false);
      setSelectedMember(null);
    }
  };
  const activeStatus = (status: number) => {
    axios
      .put(`${route}/dbAdminEditMember`, {
        member_guid: user.MemberGuid,
        session_guid: user.SessionGuid,
        data: {
          Active: status,
          MemberGuid: member.Guid
        }
      })
      .then(res => {
        console.log(res);
        if (res.data.result === 'success') {
          setGetMembers(!getMembers);
          alert.success(
            status
              ? `Activated ${member.Name} ${member.Surname}`
              : `De-activated ${member.Name} ${member.Surname}`
          );
          setOpen(false);
          setSelectedMember(null);
        } else {
          alert.error(res.data.frontEndMessage);
        }
      });
  };
  return (
    <>
      <TableRow className={classes.root}>
        <TableCell component='th' scope='row'>
          {member.Name}
        </TableCell>
        <TableCell>{member.Surname}</TableCell>
        <TableCell>{member.Email}</TableCell>
        <TableCell>
          {member.Active === 1 ? (
            <Tooltip title='De-activate' arrow>
              <IconButton onClick={() => activeStatus(0)}>
                <CheckIcon style={{ color: 'rgb(7, 221, 7)' }} />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title='Activate' arrow>
              <IconButton onClick={() => activeStatus(1)}>
                <CloseIcon color='error' />
              </IconButton>
            </Tooltip>
          )}
        </TableCell>
        <TableCell padding='checkbox'>
          {selectedMember ? (
            selectedMember === member.Guid && (
              <IconButton aria-label='expand row' onClick={save}>
                <SaveIcon />
              </IconButton>
            )
          ) : (
            <IconButton aria-label='expand row' onClick={edit}>
              <EditIcon />
            </IconButton>
          )}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={5}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box margin={1}>
              <Typography variant='h6' gutterBottom component='div'>
                Edit Member
              </Typography>
              <Table size='small' aria-label='purchases'>
                <TableBody>
                  <TableRow key={member.date}>
                    <TableCell
                      className={classes.hideBodrder}
                      component='th'
                      scope='row'
                    >
                      <TextField
                        fullWidth
                        label='Name'
                        value={name}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          setName(e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell className={classes.hideBodrder}>
                      <TextField
                        fullWidth
                        label='Surname'
                        value={surname}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          setSurname(e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell className={classes.hideBodrder}>
                      <TextField
                        fullWidth
                        label='Email'
                        value={email}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          setEmail(e.target.value)
                        }
                      />
                    </TableCell>
                    <TableCell className={classes.hideBodrder}>
                      <TextField
                        fullWidth
                        select
                        label='User Role'
                        value={role}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setRole(e.target.value as number)
                        }
                      >
                        <MenuItem value={2}>Employee</MenuItem>
                        <MenuItem value={1}>Admin</MenuItem>
                      </TextField>
                    </TableCell>
                    <TableCell className={classes.hideBodrder} />
                  </TableRow>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default ViewMembersTableData;
