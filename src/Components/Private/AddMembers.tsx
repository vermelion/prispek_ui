import React from 'react';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { TextField } from 'formik-material-ui';
import Grid from '@material-ui/core/Grid';
import { useConfig } from '../../Config/ConfigContex';
import { useAlert } from 'react-alert';
import { Check } from '@material-ui/icons';
import { AddMemberType } from '../../Config/Typescript/AddMemberType';

const AddMembers = ({
  employee,
  getMembers,
  setGetMembers,
  setAddMember
}: AddMemberType) => {
  const alert = useAlert();
  const [{ route, user }] = useConfig();
  //? Intitial Values
  const initialValues = {
    Name: '',
    Surname: '',
    Email: '',
    Role: employee ? 'else' : 'admin'
  };
  //? Validation
  const validationSchema = Yup.object({
    Name: Yup.string().required('Required'),
    Surname: Yup.string().required('Required'),
    Email: Yup.string().email('Invalid Email').required('Required')
  });
  //? Handle Submit
  const contactSubmit = (values: {
    Name: string;
    Surname: string;
    Email: string;
    Role: string;
  }) => {
    console.log('Values: ', values);
    axios
      .post(`${route}/dbAddMember`, {
        member_guid: user.MemberGuid,
        session_guid: user.SessionGuid,
        data: values
      })
      .then((res: any) => {
        console.log('AddMemberResponse: ', res);
        if (res.data.result === 'error') {
          alert.error(res.data.frontEndMessage);
          setAddMember(false);
        } else {
          setGetMembers(!getMembers);
          setAddMember(false);
          alert.success(`Added ${values.Name} ${values.Surname}`);
        }
      });
  };
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={contactSubmit}
    >
      {({ values }) => (
        <Form style={{ marginTop: '1.5rem' }}>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <Field
                component={TextField}
                name='Name'
                label='Name:'
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={3}>
              <Field
                component={TextField}
                name='Surname'
                label='Surname:'
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={6} style={{ display: 'flex' }}>
              <Field
                style={{ marginRight: '0.5rem' }}
                component={TextField}
                name='Email'
                label='Email:'
                required
                fullWidth
              />
              <IconButton type='submit' style={{ marginTop: '0.5rem' }}>
                <Check />
              </IconButton>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default AddMembers;
